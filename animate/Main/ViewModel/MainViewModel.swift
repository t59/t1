//
//  MainViewModel.swift
//  animate
//
//  Created by Ryan.Chou on 2020/11/5.
//

import UIKit

protocol ManiViewModelDelegate: class {
    func reloadData()
    func toast(_ message: String)
    func getVC() -> MainViewController
}
class MainViewModel: NSObject {

    // MARK: Variables
    let api = APIs()
    var database: Database!
    weak var delegate: ManiViewModelDelegate?
    // Datas
    var datas = [Animate.Data.Top]() { didSet { self.delegate?.reloadData() } }
    var favorites = Set<Animate.Data.Top>()
    // Current status
    var status = Status()
    // Request parameter
    lazy var parameter: Animate.Parameter = { Animate.Parameter.init(mainType: .anime, subType: .airing, page: 1) }() {
        didSet {
            loadData(parameter: self.parameter)
        }
    }

    // MARK: Setup
    func setup() {
        // 與資料庫建立連線
        database = Database()
        // 建立表（有表後不再建立）
        database.tableCreate()
        favorites = Set(read().map{$0})
    }
    // MARK: Database
    func read() -> [Animate.Data.Top] { database.query() }
    func write(_ value: Animate.Data.Top) {
        guard let malID = value.malID else {return}
        let malID64 = Int64(malID)
        database.read(malID: malID64).count > 0 ?
            database.update(malID: malID64, value: value) : database.insert(value)
    }
    func delete(_ value: Animate.Data.Top) {
        guard let malID = value.malID else {return}
        database.delete(malID: Int64(malID))
    }

    // MARK: Local Data
    func loadLocalData() {
        guard let vc = self.delegate?.getVC() else {return}
        vc.tableView.endRefreshLoad()
        datas = database.query()
    }
    // MARK: API Data
    func loadData(parameter: Animate.Parameter) {
        
        guard let mainType = parameter.mainType?.rawValue, let subType = parameter.subType?.rawValue else {return}
        api.requestGET(with: Animate.Data.self, request: .Top(type: mainType, page: parameter.page, subType: subType), headers: nil)  { [weak self] in
            guard let `self` = self else {return}
            guard let tops = $0.top, let vc = self.delegate?.getVC(), $0.status == nil else { self.delegate?.toast($0.message ?? "Loading error"); return }
            vc.tableView.endRefreshLoad()
            self.datas = self.parameter.page == 1 ? tops : self.datas + tops
            vc.tableView.hasMorePage = tops.count != 0
            vc.tableView.hasMoreData(data: tops.count, page: parameter.page, pageSize: Configurations.Constant.CITableView.pageSize)
            
        } failure: { [weak self] in
            guard let `self` = self, let vc = self.delegate?.getVC() else {return}
            vc.tableView.endRefreshLoad(discardPageNum: true)
            vc.view.makeToast($0.localizedDescription)
        }
    }
    
    func resetData(_ type: Animate.Parameter.TypeEnum? = nil, _ subType: Animate.Parameter.SubTypeEnum? = nil) {
        parameter = Animate.Parameter.init(mainType: type ?? .anime, subType: subType ?? .airing, page: 1)
        datas = []
    }
    func checkFlag(_ value: Animate.Data.Top) -> Bool {
        favorites.compactMap{ $0.malID }.contains(value.malID)
    }

}

extension MainViewModel: AnimateTableViewCellDelegate {
    func didTapFlag(_ value: Animate.Data.Top, _ method: QueryMethod) {
        switch method {
        case .delete:
            favorites = favorites.filter({$0.malID != value.malID})
            delete(value)
        default:
            favorites.insert(value)
            write(value)
        }
        print("------DIDTAPFLAG-----------")
        print(favorites.compactMap {$0.malID})
    }
}

extension MainViewModel {

    struct Constant {
        static let tag = "MainViewModel"
    }
    // MARK: Current status
    struct Status {
        var currentType: Animate.Parameter.TypeEnum = .anime
        var currentSubType: Animate.Parameter.SubTypeEnum = .airing
        var favoriteStyle: Configurations.Favorite = .all
    }
}
