//
//  Configurations.swift
//  animate
//
//  Created by Ryan.Chou on 2020/11/6.
//

import Foundation

struct Configurations {
    
    struct Constant {
        struct CITableView {
            static let pageSize: Int = 50 // 一頁應該有50筆資料
        }
    }
    enum Favorite: String, CaseIterable {
        case all = "All"
        case favorite = "Favorite"
    }
}
