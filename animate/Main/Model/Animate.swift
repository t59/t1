//
//  Anime.swift
//  animate
//
//  Created by Ryan.Chou on 2020/11/5.
//
//   var anime = try? newJSONDecoder().decode(Anime.self, from: jsonData)

import Foundation

// MARK: - Anime
struct Animate {

    // MARK: Parameter
    struct Parameter: Codable {

        var mainType: TypeEnum?
        var subType: SubTypeEnum?
        var page: Int = 1

        enum TypeEnum: String, Codable, CaseIterable {
            case anime = "anime"
            case manga = "manga"
            case peopl = "peopl"
            case characters = "characters"

            var SubType: [SubTypeEnum] {
                switch self {
                case .anime: return [.airing, .upcoming, .tv, .movie, .ova, .special, .bypopularity, .favorite]
                case .manga: return [.manga, .novels, .oneshots, .doujin, .manhwa, .manhua, .bypopularity, .favorite]
                default: return [.bypopularity, .favorite]
                }
            }
        }
        enum SubTypeEnum: String, Codable, CaseIterable {
            case airing, upcoming, tv, movie, ova, special  // Anime
            case manga, novels, oneshots, doujin, manhwa, manhua    // Manga
            case bypopularity, favorite // Both
        }
    }

    // MARK: Response
    struct Data: Codable {
        var status: Int?
        var message: String?
        var requestHash: String?
        var requestCached: Bool?
        var requestCacheExpiry: Int?
        var top: [Top]?

        enum CodingKeys: String, CodingKey {
            case status, message
            case requestHash = "request_hash"
            case requestCached = "request_cached"
            case requestCacheExpiry = "request_cache_expiry"
            case top
        }

        // MARK: - Top
        struct Top: Codable, Hashable {
            var malID, rank: Int?
            var title: String?
            var url: String?
            var imageURL: String?
            var type: String?
            var episodes: Int?
            var startDate, endDate: String?
            var members: Int?
            var score: Float?

            enum CodingKeys: String, CodingKey {

                case malID = "mal_id"
                case rank, title, url
                case imageURL = "image_url"
                case type, episodes
                case startDate = "start_date"
                case endDate = "end_date"
                case members, score
            }

        }



    }

}

extension String.StringInterpolation {
    mutating func appendInterpolation(_ value: Animate.Data.Top) {
        let image = value.imageURL ?? "nil"
        let title = value.title ?? "nil"
        let rank = value.rank ?? 0
        let startDate = value.startDate ?? "nil"
        let endDate = value.endDate ?? "nil"
        let `type` = value.type ?? "nil"
        appendInterpolation(#"""
            [rank] [title, type, sDate, eDate] [image]
            [\#(rank)][\#(title), \#(`type`), \#(startDate), \#(endDate)]
            [\#(image)]
            """#)
    }
}
