//
//  ViewController.swift
//  animate
//
//  Created by Ryan.Chou on 2020/11/5.
//

import UIKit
import FWPopupView

class MainViewController: UIViewController {
    
    // MARK: IBOutlet, Variable
    @IBOutlet weak var tableView: CITableView!
    @IBOutlet weak var btnType: UIButton!
    @IBOutlet weak var btnSubType: UIButton!
    @IBOutlet weak var btnFlagList: UIButton!
    let vm = MainViewModel()
    
    // MARK: Constant
    struct Constant {
        struct WebView {
            static let storyboard = "Main"
            static let identifier = "WebViewController"
        }
        struct Cell {
            static let identifier = "AnimateTableViewCell"
            static let height: CGFloat = 130
        }
        struct Default {
            static let type: Animate.Parameter.TypeEnum = .anime
            static let subType: Animate.Parameter.SubTypeEnum = .bypopularity
            static let favorite: Configurations.Favorite = .all
        }
    }
    // MARK: Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    func setup() {
        vm.delegate = self
        vm.setup()
        vm.loadData(parameter: self.vm.parameter)
        tableView.addRefresh(refreshHandle: { [weak self] in
            guard let `self` = self else { return }
            if self.vm.status.favoriteStyle == .all {
                self.vm.resetData(self.vm.status.currentType, self.vm.status.currentSubType)
                self.vm.loadData(parameter: self.vm.parameter)
            } else {
                self.vm.loadLocalData()
            }
        }, loadHandle: { [weak self] in
            guard let `self` = self else {return}
            if self.vm.status.favoriteStyle == .all {
                self.vm.parameter.page = self.tableView.page
                self.vm.loadData(parameter: self.vm.parameter)
            } else {
                self.vm.loadLocalData()
            }
        })
        configUI()
    }
    func configUI() {
        DispatchQueue.main.async {
            self.btnType.setTitle(self.vm.status.currentType.rawValue + "▾", for: .normal)
            self.btnSubType.setTitle(self.vm.status.currentSubType.rawValue + "▾", for: .normal)
            self.btnFlagList.setTitle(self.vm.status.favoriteStyle.rawValue + "▾", for: .normal)
        }
    }
    
    @IBAction func action(_ sender: UIButton) {
        switch sender {
        case btnType: didChangeType(sender)
        case btnSubType: didChangeSubType(sender)
        case btnFlagList: didChangeFavoriteList(sender)
        default: break
        }
    }
    
}

// MARK: Action, Delegate
extension MainViewController: ManiViewModelDelegate {
    
    func getVC() -> MainViewController { return self }
    func toast(_ message: String) { self.view.makeToast(message) }
    
    func reloadData() {
        tableView.reloadData()
        tableView.mj_header?.endRefreshing()
    }
    
    func didChangeType(_ sender: UIButton) { showMenu(sender, items: Animate.Parameter.TypeEnum.self.allCases.compactMap{ $0.rawValue }) }
    func didChangeSubType(_ sender: UIButton) { showMenu(sender, items: vm.status.currentType.SubType.compactMap { $0.rawValue }) }
    func didChangeFavoriteList(_ sender: UIButton) { showMenu(sender, items: Configurations.Favorite.allCases.compactMap { $0.rawValue }) }
    
    func showMenu(_ sender: UIButton, items: [String] = []) {
        let property = FWMenuViewProperty()
        property.popupCustomAlignment = .topCenter
        property.popupAnimationType = .scale
        property.maskViewColor = UIColor.clear
        property.touchWildToHide = "1"
        property.popupViewEdgeInsets = UIEdgeInsets(top: sender.frame.maxY + kStatusBarHeight + kNavBarHeight,
                                                    left: sender.frame.origin.x - sender.frame.size.width,
                                                    bottom: 0, right: 0
        )
        property.topBottomMargin = 10
        property.letfRigthMargin = 5
        property.animationDuration = 0.3
        property.popupArrowStyle = .round
        property.popupArrowVertexScaleX = 0.5
        property.cornerRadius = 5
        
        // 下拉的menu
        let menuView = FWMenuView.menu(itemTitles: items, itemImageNames: nil, itemBlock: { (popupView, index, title) in
            switch sender {
            case self.btnType:
                let type = Animate.Parameter.TypeEnum(rawValue: title ?? Constant.Default.type.rawValue) ?? .anime
                self.vm.status.currentType = type
                self.vm.status.currentSubType = type.SubType.first ?? .bypopularity
                self.vm.parameter = Animate.Parameter(mainType: type, subType: self.vm.status.currentSubType, page: 1)
            case self.btnSubType:
                self.vm.status.currentSubType = Animate.Parameter.SubTypeEnum(rawValue: title ?? Constant.Default.subType.rawValue) ?? .bypopularity
                self.vm.parameter.subType = self.vm.status.currentSubType
            case self.btnFlagList:
                let value = Configurations.Favorite(rawValue: title ?? Constant.Default.favorite.rawValue) ?? .all
                self.vm.status.favoriteStyle = value
                value == .all ? self.vm.loadData(parameter: self.vm.parameter) : self.vm.loadLocalData()
            default: break
            }
            self.configUI()
        }, property: property)
        menuView.show()
    }
}

// MARK: TableViewDelegate
extension MainViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int { vm.datas.count }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: Constant.Cell.identifier) as? AnimateTableViewCell else {
            return UITableViewCell()
        }
        let data = vm.datas[indexPath.row]
        cell.isFlag = vm.checkFlag(data)
        cell.data = data
        cell.delegate = vm
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return Constant.Cell.height
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let data = vm.datas[indexPath.row]
        guard let vc = UIStoryboard(name: Constant.WebView.storyboard, bundle: nil).instantiateViewController(withIdentifier: Constant.WebView.identifier) as? WebViewController else { return }
        guard let strUrl = data.url, let url = URL(string: strUrl) else {
            // MARK: TODO
            self.view.makeToast("網址錯誤"); return
        }
        vc.url = url
        vc.dismissStyle = .pop
        if let title = data.rank { vc.title = "\(title)" } else { vc.title = self.title }
        vc.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
}
