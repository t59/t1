//
//  AnitateTableViewCell.swift
//  animate
//
//  Created by Ryan.Chou on 2020/11/5.
//

import UIKit
import SDWebImage
protocol AnitateTableViewCellDelegate: class {
    func didTapFlag(_ value: Animate.Data.Top)
}
class AnimateTableViewCell: UITableViewCell {

    @IBOutlet weak var lblRank: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblType: UILabel!
    @IBOutlet weak var lblStartDate: UILabel!
    @IBOutlet weak var lblEndDate: UILabel!
    @IBOutlet weak var imgViewIcon: UIImageView!
    @IBOutlet weak var btnFlag: UIButton!

    weak var delegate: AnitateTableViewCellDelegate?

    var data: Animate.Data.Top? { didSet { setup() } }

    override func prepareForReuse() { reset() }


    func reset() {
        let elements = [lblRank, lblTitle, lblType, lblStartDate, lblEndDate, imgViewIcon]
        for element in elements { element?.isHidden = false }
    }
    func setup() {
        reset()
        if let rank = data?.rank { lblRank.text = "\(rank)" } else { lblRank.isHidden = true }
        if let title = data?.title { lblTitle.text = "\(title)" } else { lblTitle.isHidden = true }
        if let type = data?.type { lblType.text = "\(type)" } else { lblType.isHidden = true }
        if let startDate = data?.startDate { lblStartDate.text = "\(startDate)" } else { lblStartDate.isHidden = true }
        if let endDate = data?.endDate { lblEndDate.text = "\(endDate)" } else { lblEndDate.isHidden = true }
        if let imageUrl = data?.imageURL { imgViewIcon.sd_setImage(with: URL(string: imageUrl), placeholderImage: UIImage(named: "placeholder")) } else { lblRank.isHidden = true }
    }
    @IBAction func didTapFlag(_ sender: UIButton) {
        guard let data = self.data else {return}
        delegate?.didTapFlag(data)
    }

}
