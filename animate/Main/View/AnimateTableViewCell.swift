//
//  AnitateTableViewCell.swift
//  animate
//
//  Created by Ryan.Chou on 2020/11/5.
//

import UIKit
import SDWebImage

enum QueryMethod {
    case insert
    case delete
}
protocol AnimateTableViewCellDelegate: class {

    func didTapFlag(_ value: Animate.Data.Top, _ method: QueryMethod)
}
class AnimateTableViewCell: UITableViewCell {


    @IBOutlet weak var lblRank: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblType: UILabel!
    @IBOutlet weak var lblStartDate: UILabel!
    @IBOutlet weak var lblEndDate: UILabel!
    @IBOutlet weak var imgViewIcon: UIImageView!
    @IBOutlet weak var btnFlag: UIButton!

    weak var delegate: AnimateTableViewCellDelegate?

    var data: Animate.Data.Top? { didSet { setup() } }
    // No API support, use seperator variable handle
    var isFlag: Bool = false { didSet {
        let imageName = isFlag ? "bookmark.fill" : "bookmark"
        btnFlag.setImage(UIImage(named: imageName), for: .normal)
    }}

    override func prepareForReuse() { reset() }

    func reset() {
        let elements = [lblRank, lblTitle, lblType, lblStartDate, lblEndDate, imgViewIcon]
        for element in elements { element?.isHidden = false }
        btnFlag.setImage(UIImage(named: "bookmark"), for: .normal)
    }
    func setup() {
        reset()
        if let rank = data?.rank { lblRank.text = "\(rank)" } else { lblRank.isHidden = true }
        if let title = data?.title { lblTitle.text = "\(title)" } else { lblTitle.isHidden = true }
        if let type = data?.type { lblType.text = "\(type)" } else { lblType.isHidden = true }
        if let startDate = data?.startDate { lblStartDate.text = "\(startDate)" } else { lblStartDate.isHidden = true }
        if let endDate = data?.endDate { lblEndDate.text = "\(endDate)" } else { lblEndDate.isHidden = true }
        if let imageUrl = data?.imageURL { imgViewIcon.sd_setImage(with: URL(string: imageUrl), placeholderImage: UIImage(named: "placeholder")) } else { lblRank.isHidden = true }
        let imageName = isFlag ? "bookmark.fill" : "bookmark"
        btnFlag.setImage(UIImage(named: imageName), for: .normal)
    }
    @IBAction func didTapFlag(_ sender: UIButton) {
        guard let data = self.data else {return}
        delegate?.didTapFlag(data, isFlag ? .delete : .insert)
        isFlag = !isFlag
    }

}
