//
//  WebViewController.swift
//  animate
//
//  Created by 周小捷 on 2020/11/7.
//

import UIKit
import WebKit

class WebViewController: UIViewController {

    var url: URL?
    var strTitle: String?
    var dismissStyle: DismissStyle = .dismiss
    @IBOutlet weak var webView: WKWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let title = strTitle { self.title = title }
        loadData()
    }
    
    func loadData() {
        guard let url = url else { return }
        webView.load(URLRequest(url: url))
    }

}
