//
//  DBHelper.swift
//  animate
//
//  Created by 周小捷 on 2020/11/8.
//

import Foundation
import SQLite

struct Database {

    var db: Connection!

    init() {
        connectDatabase()
    }

    // 與資料庫建立連線
    mutating func connectDatabase(filePath: String = "/Documents") -> Void {

        let sqlFilePath = NSHomeDirectory() + filePath + "/db.sqlite3"

        do { // 與資料庫建立連線
            db = try Connection(sqlFilePath)
            // print("與資料庫建立連線 成功")
        } catch {
            // print("與資料庫建立連線 失敗：\(error)")
        }

    }

    // ==========================================================================
    let TABLE_LIST = Table("list") // 表名稱
    let TABLE_LIST_ID = Expression<Int64>("id") // 列表項及項型別, pk
    let TABLE_LIST_MALID = Expression<Int64>("mal_id")
    let TABLE_LIST_RANK = Expression<Int64?>("rank")
    let TABLE_LIST_TITLE = Expression<String?>("title")
    let TABLE_LIST_URL = Expression<String?>("url")
    let TABLE_LIST_IMAGE = Expression<String?>("image_url")
    let TABLE_LIST_TYPE = Expression<String?>("type")
    let TBALE_LIST_EPISODES = Expression<Int64>("episodes")
    let TABLE_LIST_STARTDATE = Expression<String?>("start_date")
    let TABLE_LIST_ENDDATE = Expression<String?>("end_date")
    let TBALE_LIST_MEMBERS = Expression<Int64?>("members")
    let TBALE_LIST_SCORE = Expression<Float64?>("score")

    // 建表
    func tableCreate() -> Void {
        do { // 建立表TABLE_LIST
            try db.run(TABLE_LIST.create { table in
                table.column(TABLE_LIST_ID, primaryKey: .autoincrement) // 主鍵自加且不為空
                table.column(TABLE_LIST_MALID)
                table.column(TABLE_LIST_RANK)
                table.column(TABLE_LIST_TITLE)
                table.column(TABLE_LIST_URL)
                table.column(TABLE_LIST_IMAGE)
                table.column(TABLE_LIST_TYPE)
                table.column(TBALE_LIST_EPISODES, defaultValue: 0)
                table.column(TABLE_LIST_STARTDATE)
                table.column(TABLE_LIST_ENDDATE)
                table.column(TBALE_LIST_MEMBERS)
                table.column(TBALE_LIST_SCORE)

            })
            // print("建立表 TABLE_LIST 成功")
        } catch {
            // print("建立表 TABLE_LIST 失敗：\(error)")
        }
    }

    // 插入
    func insert(_ value: Animate.Data.Top) {
        do {
            let rowid = try db.run(TABLE_LIST.insert(value))
            // print("插入資料成功 id: \(rowid)")
        } catch {
            // print("插入資料失敗: \(error)")
        }
    }

    // 遍歷
    @discardableResult
    func query() -> [Animate.Data.Top] {
        do {
            let values: [Animate.Data.Top] = try db.prepare(TABLE_LIST).map { try $0.decode() }
            // print("[Query ALL]", values)
            return values
        } catch {
            // print("[Query] Error = \(error)")
            return []
        }
    }

    // 讀取
    func read(malID: Int64) -> [Animate.Data.Top] {
        do {
            let values: [Animate.Data.Top] = try db.prepare(TABLE_LIST.filter(TABLE_LIST_MALID == malID)).map { try $0.decode() }
            // print("[讀取]", values)
            return values
        } catch {
            // print("[Read] Error = \(error)")
            return []
        }
    }

    // 清空
    func clean() {
        do {
            try db.run(TABLE_LIST.delete())
        } catch {
            // print("[Empty] Clean error = \(error)")
        }
    }

    // 更新
    func update(malID: Int64, value: Animate.Data.Top) -> Void {
        do {
            if try db.run(TABLE_LIST.filter(TABLE_LIST_MALID == malID).update(value)) > 0 {
                // print("\(malID) 更新成功")
            } else {
                // print("沒有發現 條目 \(malID)")
            }
        } catch {
            // print("\(malID) 更新失敗：\(error)")
        }
    }

    // 刪除
    func delete(malID: Int64) -> Void {
        let item = TABLE_LIST.filter(TABLE_LIST_MALID == malID)
        do {
            if try db.run(item.delete()) > 0 {
                // print("\(malID) 刪除成功")
            } else {
                // print("沒有發現 條目 \(malID)")
            }
        } catch {
            // print("\(malID) 刪除失敗：\(error)")
        }
    }

}
