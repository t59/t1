//
//  Config.swift
//  animate
//
//  Created by Ryan.Chou on 2020/11/5.
//

import Foundation
import UIKit

// 狀態列高度
let kStatusBarHeight: CGFloat = UIApplication.shared.statusBarFrame.size.height
// Navigationbar高度
let kNavBarHeight: CGFloat = 44.0


struct Enviroment {
    
    enum type {
        case DEV
        case PRD
    }
    static let evn: type = .PRD
    
    static var addr: String {
        switch evn {
        case .DEV: return "local"
        case .PRD: return "https://api.jikan.moe/v3"
        }
    }
}
struct Request {
    enum Target {
        case Top(type: String, page: Int, subType: String)
    }
    init(_ target: Target) {
        self.target = target
    }
    var target: Target
    var path: String {
        switch target {
        //case .Top: return "/top"
        case .Top(let type, let page, let subType): return "/top/\(type)/\(page)/\(subType)"
        }
    }
}
