//
//  APIs.swift
//  animate
//
//  Created by Ryan.Chou on 2020/11/5.
//

import UIKit
import Alamofire
import Toast_Swift

class APIs: NSObject {

    struct AlamofireManager {
        static let shared: SessionManager = {
            let configuration = URLSessionConfiguration.default
            configuration.timeoutIntervalForRequest = 30
            let sessionManager = Alamofire.SessionManager(configuration: configuration, delegate: SessionDelegate(), serverTrustPolicyManager: nil)
            return sessionManager
        }()
    }
    func decode<T: Codable>(_ parameter: T) -> [String: AnyObject]? {
        let encoder = JSONEncoder()
        let encodedData = try? encoder.encode(parameter)
        return try? JSONSerialization.jsonObject(with: encodedData!, options: []) as? [String: AnyObject]
    }
    
    func requestGET<T: Codable>(with modelType: T.Type, request: Request.Target, headers: [String: String]?, success: @escaping (T) -> Void, failure: @escaping (Error) -> Void) {
        // MARK: TODO
        let strUrl = #"\#(Enviroment.addr)\#(Request.init(request).path)"#
        print("url=", strUrl)
        guard Reachability.isConnectedToNetwork() else { failure(NSError(domain: "", code: -168, userInfo: nil)); return }
        AlamofireManager.shared.request(strUrl, headers: headers).responseJSON {
            switch $0.result {
            case .success:
                // _ = try! JSONDecoder().decode(modelType.self, from: $0.data!)
                guard let data = $0.data, let response = try? JSONDecoder().decode(modelType.self, from: data) else {
                    // MARK: TODO
                    failure(NSError(domain: "", code: -1, userInfo: nil)); return
                }
                success(response)
            case .failure(let error):
                print(error._code)
                failure(error)
            }
        }
    }
}
