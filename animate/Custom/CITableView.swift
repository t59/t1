//
//  TTableViewController.swift
//  animate
//
//  Created by Ryan.Chou on 2020/11/5.
//

import UIKit
import Foundation
import MJRefresh

class CITableView: UITableView {

    var firstPage: Int = 1  // 第一頁
    var page: Int = 1   // 當前頁面index
    var hasMorePage: Bool = true    // 有更多頁面
    var isRefreshing: Bool = false  // 刷新中
    var isLoadingMore: Bool = false // 載入中
    
    // MARK: 下拉, 上拉的handler
    //
    // refreshHandle: 刷新用
    // loadHandle: 載入用
    func addRefresh(refreshHandle: (() -> Void)?, loadHandle: (() -> Void)?) {
        if let refreshHandle = refreshHandle {
            let header = MJRefreshNormalHeader(refreshingBlock: {
                [weak self] in
                guard let `self` = self else { return }
                // 每次下拉刷新，重置 currentPage 和 hasMorePage
                self.resetRefreshConfig()
                self.isRefreshing = true
                refreshHandle()
            })
            header.stateLabel?.textColor = .lightGray
            header.stateLabel?.font = .systemFont(ofSize: 13)
            header.lastUpdatedTimeLabel?.isHidden = true
            header.arrowView?.image = nil
            self.mj_header = header
        }
        
        if let loadHandle = loadHandle {
            let footer = MJRefreshAutoNormalFooter(refreshingBlock: {
                [weak self] in
                guard let `self` = self else { return }
                self.page += 1
                self.isLoadingMore = true
                loadHandle()
            })
            footer.stateLabel?.textColor = .lightGray
            footer.stateLabel?.font = .systemFont(ofSize: 13)
            
            self.mj_footer = footer
        }
    }
    // 開始刷新
    func beginRefresh() { self.mj_header?.beginRefreshing() }
    
    // 結束刷新載入(discardPageNum: 返回當前頁數)
    public func endRefreshLoad(discardPageNum: Bool = false) {
        if discardPageNum {
            discardCurrentPage()
        }
        if isRefreshing {
            isRefreshing = false
            if self.mj_header == nil { return }
            self.mj_header?.endRefreshing()
        }
        if isLoadingMore {
            isLoadingMore = false
            self.mj_footer?.endRefreshing()
        }
    }
    
    // 返回當前頁數: 刷新載入失敗
    public func discardCurrentPage() {
        if firstPage > 0 && self.page > firstPage { self.page -= 1 }
    }
    
    // MARK: 重置刷新載入的基本設置
    //
    // currentPage：firstPage
    // hasMorePage：true
    func resetRefreshConfig() {
        self.page = self.firstPage
        self.hasMorePage = true
    }

    
    // MARK: 還有更多資料
    //
    func hasMoreData(localData: [Any]?, newData: [Any], total: Int) {
        if let data = localData, data.count > 0 {
            // 當前有資料
            if data.count >= total || newData.count == 0 {
                // 資料量大於或等於total 或者 新獲取的資料為0
                self.hasMorePage = false
                return
            }
        }
        // 還有更多
        self.hasMorePage = total != 0
    }
    
    func hasMoreData(data: Int, page: Int, pageSize: Int) {
        if page == 1 {
            if let footer = self.mj_footer as? MJRefreshAutoNormalFooter {
                footer.setTitle("", for: .idle)
                footer.setTitle("", for: .noMoreData)
            }
            self.hasMorePage = data >= pageSize
        } else {
            if let footer = self.mj_footer as? MJRefreshAutoNormalFooter {
                // MARK: TODO
                footer.setTitle("載入更多資料...", for: .idle)
                footer.setTitle("最後一頁...", for: .noMoreData)
            }
            self.hasMorePage = data >= pageSize
        }
    }
    
    func hasMoreData(totalPage: Int) { self.hasMorePage = page < totalPage }

}
