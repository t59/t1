//
//  UIViewController+Extension.swift
//  animate
//
//  Created by 周小捷 on 2020/11/7.
//

import Foundation
import UIKit

extension UIViewController {
    
    enum DismissStyle {
        case dismiss
        case pop
    }
    
}
