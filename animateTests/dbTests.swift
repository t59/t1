//
//  dbTests.swift
//  animateTests
//
//  Created by Ryan.Chou on 2020/11/9.
//

import XCTest
@testable import animate

class dbTests: XCTestCase {

    var database: Database!

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.

    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testCRUD() {
        // 與資料庫建立連線
        database = Database()
        database.connectDatabase()
        // 建立列表（有列表後不再建立）
        database.tableCreate()

        // 清空資料, D
        database.clean()
        XCTAssertEqual(0, database.query().count)

        // 插入資料, CR
        var item1 = animate.Animate.Data.Top.init(malID: 3, rank: 1, title: "title1", url: "url1", imageURL: "imageURL1", type: "type1", episodes: 1, startDate: "startDate", endDate: "endDate", members: 1, score: 1.0)
        let item2 = animate.Animate.Data.Top.init(malID: 2, rank: 2, title: "title2", url: "url2", imageURL: "imageURL2", type: "type2", episodes: 2, startDate: "startDate", endDate: "endDate", members: 2, score: 2.0)
        database.insert(item1)
        database.insert(item2)
        XCTAssertEqual(2, database.query().count)

        // 更新, U
        XCTAssertEqual(1, database.read(malID: 3).count)
        item1.title = "NewValue"
        database.update(malID: 3, value: item1)
        XCTAssertEqual(1, database.read(malID: 3).count)
        XCTAssertEqual(2, database.query().count)
        // 刪除, D
        database.delete(malID: 3)
        XCTAssertEqual(0, database.read(malID: 3).count)
        XCTAssertEqual(1, database.query().count)

    }

}
