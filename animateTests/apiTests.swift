//
//  apiTests.swift
//  animate
//
//  Created by Ryan.Chou on 2020/11/5.
//

import XCTest
@testable import animate

class apiTests: XCTestCase {

    var api: APIs!

    override func setUp() {
        super.setUp()
        api = APIs()
    }
    override func tearDown() {
        api = nil
        super.tearDown()
    }

    func testLiveAPI() {
        let expectation = self.expectation(description: "Loading")

        let p = MainViewModel().parameter
        let mainType = p.mainType ?? .anime
        let subType = p.subType ?? .airing

        let request = Request.Target.Top(type: mainType.rawValue , page: 1, subType: subType.rawValue)
        api.requestGET(with: Animate.Data.self, request: request, headers: nil) {
            XCTAssertEqual(50, $0.top?.count)
            expectation.fulfill()
        } failure: { (error) in
            XCTAssertThrowsError("Error=\(error)")
            expectation.fulfill()
        }
        waitForExpectations(timeout: 5, handler: nil)
    }

    func testStatic() {
        let testBundle = Bundle(for: type(of: self))
        let path = testBundle.path(forResource: "mock", ofType: "json")
        guard let data = try? Data(contentsOf: URL(fileURLWithPath: path!), options: .alwaysMapped) else {
            XCTAssertThrowsError("Error get json file"); return
        }
        guard let response = try? JSONDecoder().decode(Animate.Data.self, from: data), let tops = response.top else {
            XCTAssertThrowsError("Error get json file"); return
        }
        XCTAssertEqual(tops.count, 50)

    }



}
